package com.company.additionalForTask;

public class Employee {
    private String name;
    private String surname;
    private String patronymic;
    private String position;
    private double annualRate;
    private double monthRate;

    public Employee(String name, String surname, String patronymic, String position, double annualRate, double monthRate) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.position = position;
        this.annualRate = annualRate;
        this.monthRate = monthRate;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getPosition() {
        return position;
    }

    public double getAnnualRate() {
        return annualRate;
    }

    public double getMonthRate() {
        return monthRate;
    }

    public void setMonthRate(double monthRate) {
        this.monthRate = monthRate;
    }

    public void setAnnualRate(double annualRate) {

        this.annualRate = annualRate;
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + patronymic + "\n"
                + position + "\n"
                + "Годовая ЗП: " + annualRate + "\n"
                + "Месячная ЗП: " + monthRate + "\n"
                + "--------------------";
    }
}
