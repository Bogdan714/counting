package com.company.additionalForTask;

import java.util.List;

public class Accounting {
    private List<Employee> workers;

    public Accounting() {
        this.workers = Generator.generateWorkers();
    }

    public static float percentToMonthRate(float amountPerMonth, float percent) {
        return percent * amountPerMonth;
    }

    public static float dayRateToMonthRate(float dayRate) {
        return 20 * dayRate;
    }

    public static float hourRateToMonthRate(float hourRate, float hoursADay) {
        return dayRateToMonthRate(hourRate * hoursADay);
    }

    public static float baseAndPercent(float base, float percent, float amount) {
        return percentToMonthRate(amount, percent) + base;
    }

    public int setPrise(float[] sums, int parameter, String surname) {
        int index = -1;
        for (int i = 0; i < workers.size(); i++) {
            if (workers.get(i).getSurname().equalsIgnoreCase(surname)) {
                index = i;
                break;
            }
        }
        if (index < 0) {
            return -1;
        }
        float monthRate = -1;
        switch (parameter) {
            case 1://месячную
                monthRate = sums[0];
                break;
            case 2://часовую
                monthRate = hourRateToMonthRate(sums[0], sums[1]);
                break;
            case 3://процентную
                monthRate = percentToMonthRate(sums[0], sums[1]);
                break;
            case 4://процентную и базовую
                monthRate = baseAndPercent(sums[0], sums[1], sums[2]);
                break;
            case 5://дневную
                monthRate = dayRateToMonthRate(sums[0]);
                break;
        }
        workers.get(index).setMonthRate(monthRate);
        workers.get(index).setAnnualRate(monthRate * 12);
        return 0;
    }

    public List<Employee> getWorkers() {
        return workers;
    }

    public boolean hasWorker(String surname) {
        for (int i = 0; i < workers.size(); i++) {
            if (workers.get(i).getSurname().equalsIgnoreCase(surname)) {
                return true;
            }
        }
        return false;
    }
}
