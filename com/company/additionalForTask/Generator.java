package com.company.additionalForTask;

import com.company.additionalForTask.Employee;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Employee> generateWorkers() {
        List<Employee> workers = new ArrayList<>();
        workers.add(new Employee("Елена", "Иванова", "Львовна", "зам директора", 9500 * 12, 9500));
        workers.add(new Employee("Дмитрий", "Вакуленко", "Владимирович", "дизайнер", 11200 * 12, 11200));
        workers.add(new Employee("Анна", "Коренькова", "Павловна", "менеджер по продажам ", 7300 * 12, 7300));
        workers.add(new Employee("Татьяна", "Дейнеко", "Дмитриевна", "офицер полиции", 5713 * 13, 5713));
        workers.add(new Employee("Антон", "Леннон", "Давидович", "писатель", 12 * 4000, 4000));
        workers.add(new Employee("Евгений", "Курашов", "Полтавович", "верстальщик", 2 * 12, 2));
        workers.add(new Employee("Тамара", "Сало", "Петровна", "повар", 9000 * 12, 9000));
        workers.add(new Employee("Герасим", "Рыбаченко", "Филипович", "учитель", 13131 * 12, 13131));
        return workers;
    }
}
