package com.company;

import com.company.Sorters.SortByAnnualRate;
import com.company.Sorters.SortByMonthRate;
import com.company.Sorters.SortByName;
import com.company.additionalForTask.Accounting;
import com.company.additionalForTask.Employee;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Accounting accounting = new Accounting();
        int sortParam;
        while (true) {
            List<Employee> workers = accounting.getWorkers();
            do {
                System.out.println("Упорядочить по:");
                System.out.println("1 - по имени");
                System.out.println("2 - по ЗП за месяц");
                System.out.println("3 - по ЗП за год");
                sortParam = Integer.parseInt(scanner.nextLine());
            } while (sortParam < 1 || sortParam > 3);
            switch (sortParam) {
                case 1:
                    workers.sort(new SortByName());
                    break;
                case 2:
                    workers.sort(new SortByMonthRate());
                    break;
                case 3:
                    workers.sort(new SortByAnnualRate());
                    break;
            }

            for (Employee worker : workers) {
                System.out.println(worker);
            }
            String surname;
            do {
                System.out.println("Введите фамилию рабочего для изменения ЗП (exit - завершить работу)");
                surname = scanner.nextLine();
                if (surname.equalsIgnoreCase("exit")) {
                    return;
                }
                if (!accounting.hasWorker(surname)) {
                    System.out.println("Нет раба с фамилией " + surname);
                }
            } while (!accounting.hasWorker(surname));
            int choice;
            do {
                System.out.println("Выберите тип расчета:");
                System.out.println("1 - месячная ставка");
                System.out.println("2 - почасовая оплата");
                System.out.println("3 - % от объема продаж");
                System.out.println("4 - базовая месячная ставка + % от объема продаж");
                System.out.println("5 - дневная ставка");
                choice = Integer.parseInt(scanner.nextLine());
            } while (choice > 5 || choice < 1);
            float[] rateParams = new float[3];
            switch (choice) {
                case 1:
                    System.out.println("Введите месячную ставку");
                    rateParams[0] = Float.parseFloat(scanner.nextLine());
                    break;
                case 2:
                    System.out.println("Введите почасовую ставку");
                    rateParams[0] = Float.parseFloat(scanner.nextLine());
                    System.out.println("1 - в гривне, 2 - в долларах");
                    int USD1UAH2 = Integer.parseInt(scanner.nextLine());
                    if (USD1UAH2 == 2) {
                        rateParams[0] *= 26;
                        System.out.println("конвертировано в гривну..");
                    } else {
                        System.out.println("считаем в гривне..");
                    }
                    System.out.println("Сколько часов в день?");
                    rateParams[1] = Float.parseFloat(scanner.nextLine());
                    break;
                case 3:
                    System.out.println("Введите обьем продаж");
                    rateParams[0] = Float.parseFloat(scanner.nextLine());
                    System.out.println("Введите процент(0 - 100)");
                    rateParams[1] = Float.parseFloat(scanner.nextLine()) / 100;
                    break;
                case 4:
                    System.out.println("Введите базовую ставку");
                    rateParams[0] = Float.parseFloat(scanner.nextLine());
                    System.out.println("Введите процент(0 - 100)");
                    rateParams[1] = Float.parseFloat(scanner.nextLine());
                    System.out.println("Введите обьем продаж");
                    rateParams[2] = Float.parseFloat(scanner.nextLine()) / 100;
                    break;
                case 5:
                    System.out.println("Введите дневную ставку");
                    rateParams[0] = Float.parseFloat(scanner.nextLine());
                    break;
            }
            accounting.setPrise(rateParams, choice, surname);
        }
    }
}
