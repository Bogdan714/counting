package com.company.Sorters;

import com.company.additionalForTask.Employee;

import java.util.Comparator;

public class SortByMonthRate implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return (int) (o1.getMonthRate() - o2.getMonthRate());
    }
}

