package com.company.Sorters;

import com.company.additionalForTask.Employee;

import java.util.Comparator;

public class SortByAnnualRate implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return (int) (o1.getAnnualRate() - o2.getAnnualRate());
    }
}

